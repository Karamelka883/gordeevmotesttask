﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FilmsCatalog.Models;

namespace FilmsCatalog.Interface
{
    interface IMovieData
    {
        public Task<Movie> GetData(string id);
        public Task<List<Movie>> GetDataList();
        public Task DeleteData(Movie movie);
        public Task AddData(Movie movie, string UserId, string PosterPath);
        public Task UpdateData(Movie movie, string PosterPath);
    }
}
