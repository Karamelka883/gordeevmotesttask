﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FilmsCatalog.Models;
using FilmsCatalog.Interface;
using FilmsCatalog.Data;
using Microsoft.EntityFrameworkCore;

namespace FilmsCatalog.Repository
{
    public class MovieData : IMovieData
    {
        private readonly ApplicationDbContext db;

        public MovieData(ApplicationDbContext context)
        {
            db = context;
        }

        public async Task<Movie> GetData(string id)
        { 
            return await db.Movies.FirstOrDefaultAsync(p => p.Id.Equals(id));
        }

        public async Task<List<Movie>> GetDataList()
        {
            return await db.Movies.ToListAsync();
        }

        public async Task DeleteData(Movie movie)
        {
            if (movie != null)
            {
                db.Movies.Remove(movie);
               await SaveData();
            }
        }

        public async Task AddData(Movie movie, string UserId, string PosterPath)
        {
            movie.Id = Guid.NewGuid().ToString();
            movie.UserId = UserId;
            movie.Poster = PosterPath;
            db.Movies.Add(movie);
            await SaveData();  
        }

        public async Task UpdateData(Movie movie, string PosterPath)
        {
            movie.Poster = PosterPath;
            db.Movies.Update(movie);
            await SaveData();
        }

        public async Task SaveData()
        {
            await db.SaveChangesAsync();
        }
    }
}
