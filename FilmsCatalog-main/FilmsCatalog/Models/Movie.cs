﻿using System.ComponentModel.DataAnnotations;

namespace FilmsCatalog.Models
{
    public class Movie
    {
        public string Id { get; set; }

        [Required(ErrorMessage = "Обязательное поле")]
        [Display(Name = "Name")]
        public string Name { get; set; }
        public int Year { get; set; }
        public string Description { get; set; }
        public string Director { get; set; }
        public string Poster { get; set; }
        public string UserId { get; set; }
    }
}