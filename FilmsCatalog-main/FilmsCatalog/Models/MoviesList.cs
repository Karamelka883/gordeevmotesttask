﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FilmsCatalog.Models
{
    public class MoviesList
    {
        public List<Movie> Movies { get; set; }
         public int CurrentPageIndex { get; set; }
        public int PageCount { get; set; }
    }
}
