﻿using FilmsCatalog.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Identity;
using FilmsCatalog.Data;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using FilmsCatalog.Interface;
using FilmsCatalog.Repository;
using Microsoft.AspNetCore.Authorization;
using ReflectionIT.Mvc.Paging;

namespace FilmsCatalog.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        IWebHostEnvironment _appEnvironment;
        private IMovieData imoviedata;
        private readonly ApplicationDbContext db;

        public HomeController(ILogger<HomeController> logger,
            UserManager<User> userManager, SignInManager<User> signInManager,
            ApplicationDbContext context, IWebHostEnvironment appEnvironment)
        {
            _logger = logger;
            _userManager = userManager;
            _signInManager = signInManager;
            imoviedata = new MovieData(context);
            _appEnvironment = appEnvironment;
            db = context;
        }

        public async Task<IActionResult> Index(int page = 1)
        {
            if (User.Identity.IsAuthenticated)
            {
                var data = db.Movies.AsNoTracking().OrderBy(p => p.Name);
                var model = await PagingList.CreateAsync(data, 15, page);
                return View(model);
            }

            else
                return RedirectToAction("SignIn", "Account");
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpGet]
        public async Task<IActionResult> Movie(string id) // Карточк фильма
        {
            if (id != null)
            {
                Movie model = await imoviedata.GetData(id);
                return View(model);
            }
            else
                return RedirectToAction("Index");

        }

        [HttpGet]
        public async Task<IActionResult> EditData(string id) // Редактирование карточки фильма
        {

            Movie model;

            if (id == null)
            {
                model = new Movie();
            }
            else
            {
                model = await imoviedata.GetData(id);
            }

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> SaveData(Movie data, IFormFile uploadedFile) // Сохранение данных
        {
            // Загружаем файл и сораняем его на сервере, т.к. в бд правильно хранить ссылки на изображения
            string PostersPath;
            if (uploadedFile != null)
            {
                PostersPath = "/Posters/" + uploadedFile.FileName;
                using (var fileStream = new FileStream(_appEnvironment.WebRootPath + PostersPath, FileMode.Create))
                {
                    await uploadedFile.CopyToAsync(fileStream);
                }
            }
            else
                PostersPath = "/Images/PosterDefault.png";
            if (data.Id == null)
            {
                string UserId = _userManager.GetUserId(User);
                await imoviedata.AddData(data, UserId, PostersPath);
            }
            else
            {
                if (!PostersPath.Equals(data.Poster) && !PostersPath.Equals("/Images/PosterDefault.png")) 
                    await imoviedata.UpdateData(data, PostersPath);
                else
                    await imoviedata.UpdateData(data, data.Poster);
            }
            return RedirectToAction("Index");

        }

        [HttpPost]
        public async Task<IActionResult> DeleteData(string id)
        {
            if (id != null)
            {
                Movie movie = await imoviedata.GetData(id);
                if (movie != null)
                {
                    await imoviedata.DeleteData(movie);
                    return RedirectToAction("Index");
                }
            }
            return NotFound();
        }
    }
}
